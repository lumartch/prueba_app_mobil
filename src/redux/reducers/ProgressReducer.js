import { UPDATE_PROGRESS, LOAD_PROGRESS, CLEAR_PROGRESS } from "../actions/ProgressActions"
import INDEX_JSON_LESSONS from '../../lessons-json/index-lessons.json';

const default_progress_state = {
    progress: INDEX_JSON_LESSONS,
    current: "Sin internet",
    finished: 0,
    achievements: 0
}

const current_progress = (progress) => {
    for(var header in progress){
        for(var lesson in progress[header]){
            if(progress[header][lesson] === false)
                return { header: header, lesson: lesson }
        }
    }
    return "Lecciones terminadas";
}

const finished_lessons = (progress) => {
    let count = 0
    for(var header in progress){
        for(var lesson in progress[header]){
            if(progress[header][lesson] === true){
                count++
            }
        }
    }
    return count;
}

const achievements_lessons = (progress) => {
    let count = 0
    for(var header in progress){
        for(var lesson in progress[header]){
            if(progress[header][lesson] === true){
                count++
            }
        }
    }
    return count/5 === 0 ? 0 : Math.trunc(count/5 + 1);
}

const progress_state = (state = default_progress_state, action) => {
    switch (action.type) {
        case UPDATE_PROGRESS:
            let json_index = state.progress
            json_index[action.field][action.theme] = true; 
            return {
                progress: json_index,
                current: current_progress(json_index),
                finished: finished_lessons(json_index),
                achievements: achievements_lessons(json_index)
            }

        case LOAD_PROGRESS: 
            return {
                progress: action.data,
                current: current_progress(action.data),
                finished: finished_lessons(action.data),
                achievements: achievements_lessons(action.data)
            }
        
        case CLEAR_PROGRESS:
            return {
                progress: {},
                current: "Sin internet",
                finished: 0,
                achievements: 0
            }

        default:
            return state
    }
}

export default progress_state;