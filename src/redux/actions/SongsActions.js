export const FINISHED_SONG = 'FINISHED_SONG';
export const FAVORITE_SONG = 'FAVORITE_SONG';
export const LOAD_SONGS = 'LOAD_SONGS';
export const CLEAR_SONGS = 'CLEAR_SONGS';

export const finished = (id) => {
    return {
        type: FINISHED_SONG,
        id: id
    }
}

export const favorite = (id, value) => {
    return {
        type: FAVORITE_SONG,
        id: id,
        value: value
    }
}

export const clear = () => {
    return {
        type: CLEAR_SONGS
    }
}

export const load = (data) => {
    return {
        type: LOAD_SONGS,
        data: data
    }
}

export const updateFinishedSong = (id) => {
    return(dispatch) => {
        dispatch(finished(id))
    }
}

export const updateFavoriteSong = (id, value) => {
    return(dispatch) => {
        dispatch(favorite(id, value))
    }
}

export const loadSongs = (json) => {
    return (dispatch) => {
        dispatch(load(json))
    }
}

export const clearSongs = () => {
    return (dispatch) => {
        dispatch(clear())
    }
}
