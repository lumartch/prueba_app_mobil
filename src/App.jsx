import React, { Component } from 'react';
import { BrowserRouter as Router, Redirect, Route } from 'react-router-dom';
import { IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

// Game
import Game from './pages/game/Game';
import MusicLibrary from './pages/game/MusicLibrary';
import PianoSolo from './pages/game/PianoSolo';
// Lecciones
import LessonsPage from "./pages/lessons/LessonsPage";
import LessonsContent from "./pages/lessons/LessonsContent";
import LessonsActivity from "./pages/lessons/LessonsActivity";
// Login
import LoginPage from './pages/login/LoginPage';
import NewAccountPage from './pages/login/NewAccountPage';
import RecoverPasswordPage from './pages/login/RecoverPasswordPage';
// Menú
import SideMenu from './components/menu/SideMenu';
// Profile 
import ProfilePage from './pages/profile/ProfilePage';
// Settings
import SettingsPage from './pages/settings/SettingsPage';
// Not found page
import NotFound from './pages/notFound/NotFound';
// Home
import Home from './pages/Home';
// Datos persistentes
import { connect } from 'react-redux';

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";

/* Import de los Fonts */
import "fontsource-kalam";
import "fontsource-amatic-sc";

class App extends Component {
  
  render(){
    return(
      <Router>
        <IonReactRouter>
          <IonSplitPane contentId="Main" when="@media (min-width: 2200px)">
            <SideMenu/>
            <IonRouterOutlet id="Main" mode="md">
              <Route exact path="/lessonscontent" component={ LessonsContent }/>
              <Route exact path="/lessonsactivity" component={ LessonsActivity }/>
              <Route exact path="/home" component={ Home }/>
              <Route exact path="/lessons" component={ LessonsPage }/>
              <Route exact path="/game" component={ Game }/>
              <Route exact path="/pianosolo" component={ PianoSolo }/>
              <Route exact path="/library" component={ MusicLibrary }/>
              <Route exact path="/profile" component={ ProfilePage }/>
              <Route exact path="/settings" component={ SettingsPage  }/>
              <Route exact path="/login" component={ LoginPage }/>
              <Route exact path="/new" component={ NewAccountPage}/>
              <Route exact path="/recover" component={ RecoverPasswordPage }/>
              <Route exact path="/" render={() => this.props.user_state.login ? <Redirect to="/home"/> :  <Redirect to="/login"/>} /> 
              <Route component={NotFound}/>
            </IonRouterOutlet>
          </IonSplitPane>
        </IonReactRouter>
      </Router>
    )
  }
}

const mapStateToProps = state => {
  return {
      user_state: state.user_state
  }
}

export default connect(mapStateToProps)(App);
