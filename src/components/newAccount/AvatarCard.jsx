import React, { Component } from 'react';
import {IonItem, IonCard, IonCardContent, IonLabel, IonCardHeader} from '@ionic/react'
import Avatar from "./Avatar.jsx"
import './NewAccountAvatar.css';

class AvatarCard extends Component {
    render() {
        return (
            <div className="container">                            
                <IonCard className={ this.props.style }
                style={{ backgroundColor: this.props.color }}>
                    <IonCardHeader> 
                        <IonItem className="back-ground" lines="none" 
                            button onClick= { () => this.props.handleChangeSrc(this.props.path, this.props.name) }>
                            <Avatar 
                                src={ this.props.path } 
                            />
                        </IonItem>
                    </IonCardHeader>
                    <IonCardContent className="avatar-card-content">
                        <IonLabel><strong>Tamaño: </strong> {this.props.size} </IonLabel>
                        <br/>
                        <IonLabel><strong>Color Favorito:</strong> {this.props.favColor} </IonLabel>
                        <br/>
                        <IonLabel><strong>Descripción: </strong>{this.props.description} </IonLabel>
                        <br/>
                        <IonLabel><strong>Pasatiempo:</strong> {this.props.hobby} </IonLabel>
                        <br/>
                        <IonLabel><strong>Gustos: </strong>{this.props.loves} </IonLabel>
                        <br/>
                        <IonLabel><strong>Desagrada: </strong>{this.props.hates} </IonLabel>
                    </IonCardContent>
                </IonCard> 
            </div>
        );
    }
}

export default AvatarCard;