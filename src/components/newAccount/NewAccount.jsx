import React, {Component} from 'react';
import { IonGrid, 
        IonItem, 
        IonLabel, 
        IonInput, 
        IonRow, 
        IonCol, 
        IonButton, 
        IonCheckbox, 
        IonAlert } from '@ionic/react';
import SecretQuestion from './SecretQuestion';
import NewAccountAvatar from './NewAccountAvatar';
import { connect } from 'react-redux';
import store from '../../redux/store'
import { fetchLogin } from '../../redux/actions/LoginActions';
import { FetchNewAccount } from './NewAccountConst';
import { LessonsFetch } from '../lessonComponents/LessonsConst';
import { fetchSongs } from '../songComponents/SongListConst';
import { loadProgress } from '../../redux/actions/ProgressActions';
import { loadSongs } from '../../redux/actions/SongsActions';
import './NewAccount.css';
import { withRouter } from 'react-router';
import { emailFetch } from './EmailFetch';

let registeredButton = false;
class NewAccount extends Component{
    constructor(){
        super()
        this.state = {
            newEmail: undefined,
            newPass: undefined,
            confirmPass: undefined,
            newName: undefined,
            newPhone: undefined,
            newNickName: undefined,
            invalidEmail: false,
            messageError: undefined,
            confirmTerms: false,
            showAlert: false,
            alertMessage: undefined,
            messageErrorPhone: undefined, 
            invalidPhone: false,
            secretQuestion: undefined,
            secretAnswer: undefined,
            myAvatar: "cat",
            pathAvatar: "assets/avatars/cat.jpg"
        }
    }
    _handleSecretQuestion = (secretQuestion) => {
        if(secretQuestion !== undefined){
            this.setState({ secretQuestion: secretQuestion })
        }
    }

    _handleSecretAnswer = (e) => {
        this.setState({ secretAnswer: e.target.value })
    }

    _handleNameChange = (e) => {
        this.setState({ newName: e.target.value })
    }

    _handleBlurName = (e) => {
        if (!e.target.value.replace(/\s/g, '').length) {
            console.log('string only contains whitespace (ie. spaces, tabs or line breaks)');
        }
    }

    _handleNicknameChange = (e) => {
        this.setState({ newNickName: e.target.value })
    }

    _handleEmailChange = (e) => {
        this.setState({ newEmail: e.target.value })
    }

    _handlePassChange = (e) => {
        this.setState({ newPass: e.target.value })
    }

    _handlePhoneChange = (e) => {
        this.setState({newPhone: e.target.value})
    }
    _handleConfirmPass = (e) => {
        this.setState({ confirmPass: e.target.value })
    }

    _handleAcceptChange = () => {
        this.setState({ confirmTerms: !this.state.confirmTerms });
    }

    _handlePhoneBlur = (e) => {
        let rx = new RegExp("\\d{10}$");
        if (rx.test(e.target.value)){
            this.setState({invalidPhone: false})
        }
        else {
            this.setState({
                    invalidPhone: true,
                    messageErrorPhone: "Favor de escribir correctamente tu número de teléfono."
            })
        }
    }

    _handleEmailBlur = async (e) => {
        let rx = new RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);
        if(rx.test(e.target.value)){
            // Verificamos la disponibilidad del correo
            let response = await emailFetch(this.state.newEmail)
            if (response["invalidEmail"] === false){
                this.setState({ invalidEmail: false })
            } else {
                this.setState({ 
                    invalidEmail: true, 
                    messageError: response["messageError"]
                })
            }
        } else {
            if(e.target.value !== ''){
                this.setState({ 
                    invalidEmail: true, 
                    messageError: "El correo está mal escrito, revisalo por favor. :3"
                })
            }
        }
    }

    _handleSubmit = async(e) => {
        e.preventDefault()
        const {confirmTerms, newPass, confirmPass, 
            invalidEmail, invalidPhone, secretQuestion, 
            secretAnswer, newName, newNickName, 
            myAvatar, newEmail, newPhone } = this.state
        let invalidName = newName.replace(/\s/g, '').length === 0
        let invalidNickname = newNickName.replace(/\s/g, '').length === 0
        let invalidPass = newPass.replace(/\s/g, '').length === 0
        let flag = confirmTerms === true && (newPass === confirmPass)
                && invalidEmail === false && invalidPhone === false
                && secretQuestion !== undefined && !invalidName && !invalidNickname
                && !invalidPass
        
        if(flag){
            console.log("Submit")
            registeredButton = true
            let response = await FetchNewAccount(newEmail, newPass, 
                newNickName, newName, newPhone, 
                secretQuestion, secretAnswer, myAvatar)
            if(response["state"] !== false){
                let progress_state = await LessonsFetch(newEmail)
                let songs_state = await fetchSongs(newEmail, "")
                store.dispatch(loadProgress(progress_state))
                store.dispatch(loadSongs(songs_state))
                store.dispatch(fetchLogin(response))
                this.props.history.push("/home")
            } else {
                registeredButton = false
            }
        } else if(newPass !== confirmPass){
            this.setState({
                showAlert: true,
                alertMessage: '<label class="error-alert-message">Las contraseñas no coinciden, reviselas por favor.</label>'
            })
        } else if (confirmTerms === false) {
            this.setState({
                showAlert: true,
                alertMessage: '<label class="error-alert-message">Acepta los términos y condiciones para continuar.</label>'
            })
        } else if (invalidEmail) {
            this.setState({
                showAlert: true,
                alertMessage: '<label class="error-alert-message">Favor de ingresar un correo válido.</label>'
            })
        } else if (invalidPhone) {
            this.setState({
                showAlert: true,
                alertMessage: '<label class="error-alert-message">Favor de ingresar un teléfono válido.</label>'
            })
        } else if (secretQuestion === undefined) {
            this.setState({
                showAlert: true,
                alertMessage: '<label class="error-alert-message">Favor de elegir una pregunta secreta.</label>'
            })
        } else if(invalidName || invalidNickname || invalidPass){
            this.setState({
                showAlert: true,
                alertMessage: '<label class="error-alert-message">No se pueden ingresar solo espacios en los campos.</label>'
            })
        }
    }

    _setAvatar = (avatar) => {
        this.setState({ myAvatar: avatar, pathAvatar: "assets/avatars/" + avatar + ".jpg" })
    }

    render(){
        const { invalidEmail, 
            messageError, 
            alertMessage, 
            showAlert, 
            messageErrorPhone, 
            invalidPhone, pathAvatar } = this.state
        const terminos = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
        return (
            <form onSubmit = {this._handleSubmit}>
                <IonAlert
                        isOpen={ showAlert }
                        onDidDismiss={ () => this.setState({ showAlert: false }) }
                        backdropDismiss = { false }
                        cssClass = 'alert-register'
                        header = { "" }
                        subHeader = { "" }
                        message = { alertMessage }
                        buttons = { ["Ok"] }
                    />
                <NewAccountAvatar getAvatar={ this._setAvatar } myAvatar={ pathAvatar } ></NewAccountAvatar>
                <div className="select-avatar-div"><label> Elige tu avatar</label></div>
                <IonItem className="newAccount-background" lines="none">
                    <IonLabel className="newAccount-label"position="floating"> Nombre </IonLabel>
                    <IonInput 
                        required
                        className="input-back-ground"
                        type="text" 
                        placeholder="Juan"
                        onIonChange={this._handleNameChange}> 
                    </IonInput>
                </IonItem>

                <IonItem className="newAccount-background" lines="none">
                    <IonLabel  className="newAccount-label" position="floating"> Apodo </IonLabel>
                    <IonInput 
                        required
                        className="input-back-ground"
                        type="text" 
                        placeholder="Champion"
                        onIonChange={this._handleNicknameChange}>
                    </IonInput>
                </IonItem>

                <IonItem className="newAccount-background" lines="none">
                { invalidPhone ? <label className="email-alert">{ messageErrorPhone }</label>: null }
                <IonLabel  className="newAccount-label" position="floating"> Teléfono </IonLabel>
                    <IonInput 
                        required
                        className="input-back-ground"
                        type="text"
                        placeholder="5544332211"
                        maxlength="10"
                        onIonBlur = { this._handlePhoneBlur }
                        onIonChange = { this._handlePhoneChange}> 
                    </IonInput>
                </IonItem>

                <IonItem className="newAccount-background" lines="none">
                    { invalidEmail ? <label className="email-alert">{ messageError }</label>: null }
                    <IonLabel  className="newAccount-label" position="floating"> Email </IonLabel>
                    <IonInput 
                        required
                        id="email-input"
                        className="input-back-ground"
                        type="email"
                        ref={this.textInput}
                        placeholder="ejemplo@correo.com"
                        onIonChange={ this._handleEmailChange }
                        onIonBlur={ this._handleEmailBlur }
                        > 
                    </IonInput>
                </IonItem>

                <IonItem className="newAccount-background" lines="none">
                    <IonLabel  className="newAccount-label" position="floating" > Contraseña </IonLabel>
                    <IonInput 
                        clearOnEdit="false"
                        required
                        className="input-back-ground"
                        type="password" 
                        placeholder="Escribe tu contraseña"
                        onIonChange={this._handlePassChange}> 
                    </IonInput>
                </IonItem>

                <IonItem className="newAccount-background" lines="none">
                    <IonLabel className="newAccount-label" position="floating"> Confirmación de contraseña </IonLabel>
                    <IonInput 
                        required
                        clearOnEdit="false"
                        className="input-back-ground"
                        type="password" 
                        placeholder="Confirma tu contraseña"
                        onIonChange={this._handleConfirmPass}> 
                    </IonInput>
                </IonItem>

                <SecretQuestion func={this._handleSecretQuestion} question={this.state.secretQuestion}></SecretQuestion>

                <IonItem className="newAccount-background" lines="none">
                    <IonLabel className="newAccount-label"position="floating"> Respuesta </IonLabel>
                    <IonInput 
                        required
                        className="input-back-ground"
                        type="text" 
                        placeholder="Ej: Rojo, Firulais..."
                        onIonChange={this._handleSecretAnswer}> 
                    </IonInput>
                </IonItem>

                <div>
                    <IonItem className="newAccount-background item-checkbox" lines="none">
                        <IonCheckbox className="terminos-checkbox" color="primary" slot="start" onClick={this._handleAcceptChange}></IonCheckbox> 
                    </IonItem> 
                    <IonItem button className="newAccount-background accept-message" lines="none" onClick={() => this.setState({showAlert: true,  alertMessage: '<label class="error-alert-message">' + terminos + '</label>' })}>
                        Acepta nuestros terminos y condiciones de uso de la aplicación.
                    </IonItem>
                </div>
                
                <IonGrid>
                    <IonRow>
                        <IonCol>
                            <IonButton 
                            className="submit-button"
                            round
                            type="submitButton" 
                            expand="block"
                            disabled={registeredButton}
                            > ¡Registrarme! </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </form>
        )
    }
}

export default withRouter(connect()(NewAccount));