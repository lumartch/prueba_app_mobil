import React, { Component } from 'react';
import { IonSelect, IonSelectOption } from '@ionic/react';
import "./SecretQuestion.css"


class SecretQuestion extends Component {
    constructor(){
        super()
        this.state = {
            disabled: false,
            question: undefined
        } 
    }

    componentDidMount() {
        if(this.props.disabled === true && this.props.question !== undefined){
            this.setState({
                disabled: this.props.disabled,
                question: this.props.question
            })
        } else {
            
        }
    }

    _setQuestion = (e) => {
        try {
            this.props.func(e.detail.value)
        } catch (err){
            console.log(err)
        }
    }

    render() {
        const { disabled } = this.state
        const { question } = this.props
        return (
            <IonSelect className="question-select" 
                placeholder="Pregunta secreta" 
                onIonChange={this._setQuestion} 
                disabled={disabled}
                value={question}
                lines="none">
                <IonSelectOption className="question-option" value={0}>¿Quién es tu mejor amigo?</IonSelectOption>
                <IonSelectOption className="question-option" value={1}>¿Cuál es el nombre de tu mascota?</IonSelectOption>
                <IonSelectOption className="question-option" value={2}>¿Cuál es el nombre de tu película favorita?</IonSelectOption>
                <IonSelectOption className="question-option" value={3}>¿Cuál es tu color favorito?</IonSelectOption>
            </IonSelect>
        );
    }
}

SecretQuestion.defaultProps = {
    disbled: false,
}

export default SecretQuestion;