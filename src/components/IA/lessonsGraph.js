export class Graph {
    constructor() {
        this.vertexList = []
    }

    CreateGraph(json) {
        Object.entries(json).forEach((lessons) => {
            Object.entries(lessons[1]).forEach((entry) => {
                this.addVertex(new Vertex(entry[0]))
            })
        })
        // 0 - 6 Lazos 
        for(let i = 0; i <= 6; i++){
            this.vertexList[i].addEdge(new Edge(this.vertexList[i].label))
        }
        
        // 1 -> 0
        this.vertexList[1].addEdge(new Edge(this.vertexList[0].label))
        // 3 -> 2
        this.vertexList[3].addEdge(new Edge(this.vertexList[2].label))
        // 8 -> 7 Intervalos de segunda - Intro, 4, 0 - 3
        this.vertexList[8].addEdge(new Edge(this.vertexList[4].label))
        // 9 -> 7 Intervalos de segunda - Intro, 4, 0 - 3
        this.vertexList[9].addEdge(new Edge(this.vertexList[4].label))
        // 10 -> 7 Intervalos de segunda - Intro, 5, 0 - 3
        this.vertexList[10].addEdge(new Edge(this.vertexList[5].label))
        // 
        for (let i = 8; i < 11; i++) {
            
            for (let j = 0; j < 4; j++) {
                this.vertexList[i].addEdge(new Edge(this.vertexList[j].label))
            }
            this.vertexList[i].addEdge(new Edge(this.vertexList[7].label))       
        }
        // 11 -> 5, 8, 9
        this.vertexList[11].addEdge(new Edge(this.vertexList[5].label))
        this.vertexList[11].addEdge(new Edge(this.vertexList[8].label))
        this.vertexList[11].addEdge(new Edge(this.vertexList[9].label))
    
        // 12 -> 4, 8, 9
        this.vertexList[12].addEdge(new Edge(this.vertexList[4].label))
        this.vertexList[12].addEdge(new Edge(this.vertexList[8].label))
        this.vertexList[12].addEdge(new Edge(this.vertexList[9].label))
    
    
        // 13 -> 6
        this.vertexList[13].addEdge(new Edge(this.vertexList[6].label))
        // 14 -> 6
        this.vertexList[14].addEdge(new Edge(this.vertexList[6].label))
        // 15 -> 4, 8, 9
        this.vertexList[15].addEdge(new Edge(this.vertexList[4].label))
        this.vertexList[15].addEdge(new Edge(this.vertexList[8].label))
        this.vertexList[15].addEdge(new Edge(this.vertexList[9].label))
        // 16 -> 5
        this.vertexList[16].addEdge(new Edge(this.vertexList[5].label))
        this.vertexList[16].addEdge(new Edge(this.vertexList[8].label))
        this.vertexList[16].addEdge(new Edge(this.vertexList[9].label))
        // 17 -> 6
        this.vertexList[17].addEdge(new Edge(this.vertexList[6].label))
        // 18 -> 12, 14, 4, 8
        this.vertexList[18].addEdge(new Edge(this.vertexList[4].label))
        this.vertexList[18].addEdge(new Edge(this.vertexList[8].label))
        this.vertexList[18].addEdge(new Edge(this.vertexList[14].label))
        // 19 -> 12, 14, 4, 9
        this.vertexList[19].addEdge(new Edge(this.vertexList[4].label))
        this.vertexList[19].addEdge(new Edge(this.vertexList[9].label))
        this.vertexList[19].addEdge(new Edge(this.vertexList[14].label))
        // 20 -> 12, 14, 5
        this.vertexList[20].addEdge(new Edge(this.vertexList[5].label))
        // 21 -> 12, 14, 5
        this.vertexList[21].addEdge(new Edge(this.vertexList[5].label))
        for(let i = 18; i < 22; i++){
            this.vertexList[i].addEdge(new Edge(this.vertexList[12].label))  
        }
        //console.log(g)
        //this.Dijkstra(0)
    }

    addVertex(vertex) {
        this.vertexList.push(vertex)
    }

    getEdges(lesson){
        for(var i = 0; i < this.vertexList.length; i++){
            if(lesson === this.vertexList[i].label){
                var list = []
                for(var j = 0; j < this.vertexList[i].edges.length; j++){
                    list.push(this.vertexList[i].edges[j].destination)
                }
                return list
            }
        }
        return []
    } 
}


class Vertex {
    constructor(label) {
        this.label = label
        this.edges = []
    }

    addEdge(edge){
        this.edges.push(edge)
    }
}

class Edge {
    constructor(destination){
        this.destination = destination
        this.weight = 1
    }
}

