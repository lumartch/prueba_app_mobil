import { LINK_SERVER, IS_LOCAL } from '../../Const';

export const loginRequest = (email, password) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();
        var url = IS_LOCAL ? LINK_SERVER + "/login/" : LINK_SERVER + "/login/login.php";
        xhttp.open("POST", url, true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                if (xhttp.responseText === "{}"){
                    resolve( { alert: true, msg: "Error al ingresar correo o contraseña."  })
                } else {
                    let json = JSON.parse(xhttp.responseText)
                    json.correo = email
                    resolve(json)
                }
            } else {
                resolve({ alert: true, msg: "No sé pudo efectuar la consulta, intente mas tarde."  });
            }
        };
        var data = JSON.stringify({ "correo": email, "pass": password });
        xhttp.send(data);
    })
}