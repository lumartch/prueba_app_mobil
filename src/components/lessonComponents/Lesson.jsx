import React, { Component } from 'react'
import { 
        IonItem, 
        IonIcon,
        IonProgressBar,
        IonCardContent} from '@ionic/react';
import { trophyOutline, trophy } from 'ionicons/icons';
import { connect } from 'react-redux';
import PropTypes from'prop-types';
import IndividualLesson from './IndividualLesson';
import './Lesson.css';

class Lesson extends Component {
    constructor(){
        super()
        this.state = {
            flag: false,
            content: [],
            noLesson: "",
            value: 0,
            colorHeader: "",
            colorProgress: "", 
            colorLabel: ""
        }
    }

    _handleClick = () => {
        this.setState({ flag: !this.state.flag })
    }

    _fillContent = () => {
        return this.props.content.map( (lesson, key) => {
            return <IndividualLesson key={key} 
                header={ this.props.noLesson } 
                lesson={ lesson[0] } 
                isFinished={ lesson[1] }
                disabled={ lesson[0] === this.props.progress_state.current.lesson ? true : lesson[1] }
                />
        })
    }

    componentDidMount(){
        this._updateVisualProgress()
    }

    _updateVisualProgress = () => {
        var cFinished = 0
        var colorHeader = "unblock-background-color-header"
        var colorProgress = "unblock-background-color-progress"
        var colorLabel = "light-blue"

        this.props.content.forEach(v => {
            if (v[1] === true) {
                cFinished += 1
            }
        })

        if (cFinished === 0){
            colorHeader = "block-background-color-header"
            colorProgress = "block-background-color-progress"
            colorLabel = "white"
        }
        this.setState({
            value: cFinished / this.props.content.length,
            colorHeader: colorHeader,
            colorProgress: colorProgress,
            colorLabel: colorLabel,
        })
    }

    componentDidUpdate(prevProps){
        if(prevProps.content !== this.props.content){
            this._updateVisualProgress()
        }
    }

    render() {
        const { flag, value, colorHeader, colorProgress, colorLabel } = this.state;
        return (
            <div className="lesson-card" >
                <IonItem className="lesson-item" lines="none" button onClick={this._handleClick} >
                    <div className={"lesson-header " +  colorHeader}>
                        <label className="lesson-name" color={colorLabel}>  
                            { this.props.noLesson } 
                        </label>
                        <div className={"lesson-progress " + colorProgress}>
                        {
                            value === 1
                            ? <IonIcon className="lesson-progress-icon" icon={ trophy }/>
                            : <IonIcon className="lesson-progress-icon" icon={ trophyOutline }/>
                        }
                        </div>      
                    </div>
                </IonItem>
                <div className="bar-container">
                    <IonProgressBar class="lesson-progress-bar" value={value} ></IonProgressBar>
                </div>
                { 
                    flag 
                    ?   <IonCardContent className="lesson-content"> 
                            { 
                                this._fillContent() 
                            } 
                        </IonCardContent>
                    : null
                }
            </div>
        )
    }
}

Lesson.propType = {
    noLesson: PropTypes.string.isRequired,
    content: PropTypes.array.isRequired
}

const mapStateToProps = state => {
    return {
        progress_state: state.progress_state
    }
}

export default connect(mapStateToProps)(Lesson)