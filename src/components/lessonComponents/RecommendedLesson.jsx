import { IonItem } from '@ionic/react'
import React, { Component } from 'react'
import './RecommendedLesson.css'
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Graph } from '../IA/lessonsGraph';
import { Tree } from '../IA/lessonsTree';
import  RecommendedImg  from '../../images/Leccion_recomendada.png'

class RecommendedLesson extends Component {
    constructor() {
        super()
        this.state = {
            recommendedTitle: "",
            recommendedHeader: "", 
            showLesson: false,
        }
    }

    _recommendButton = () => {
        if(!this.state.showLesson){
            var graph = new Graph()
            graph.CreateGraph(this.props.lessons)
            var tree = new Tree()
            tree.buildTree(this.props.lessons)
            tree.updateTree()
            var relatedLessons = graph.getEdges(this.props.progress_state.current.lesson)
            var recommendedLesson = tree.recommendLesson(relatedLessons)
            var recommendedHeader = tree.findCategory(recommendedLesson, tree.Nodes[0])
            this.setState({recommendedTitle: recommendedLesson.label, recommendedHeader: recommendedHeader, showLesson: true})
        }
        else {
            this.setState({showLesson: false})
        }
        
    }


    render() {
        return (
                <div className="recommended-card">
                <IonItem className="recommended-item" lines="none" button onClick={this._recommendButton}>
                    <div className={this.state.showLesson 
                                    ? "recommended-div-header recommend-border-cero" : "recommended-div-header"}>
                        <label className="name-label"> Lección Recomendada </label>
                    </div>   
                </IonItem>

                {
                    this.state.showLesson 
                    ?   <Link 
                            to={{
                                pathname:`/lessonscontent`,
                                state: {
                                    lesson: this.state.recommendedTitle, 
                                    header: this.state.recommendedHeader
                                }
                            }} 
                            className="link-lesson"
                        >
                            <IonItem className="recommended-item" lines="none">
                                <div className="recommended-content-background">
                                    <div className="recommended-div-container"> 
                                        <img alt={RecommendedImg} className="recommended-img" src={RecommendedImg}></img>
                                    </div>
                                    <div className="recommended-div-container"> 
                                        <label className="title-label title-position"> {this.state.recommendedTitle} </label>
                                    </div>
                                </div>
                            </IonItem>
                        </Link>
                    :   null

                }                
          </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state,
        progress_state: state.progress_state,
        lessons: state.lessons_state.lessons
    }
  }


export default withRouter(connect(mapStateToProps)( RecommendedLesson))