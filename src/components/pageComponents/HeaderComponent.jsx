import React, { Component } from 'react';
import { IonTitle,  IonLabel} from '@ionic/react';
import './HeaderComponent.css';

class HeaderComponent extends Component {
    constructor() {
        super()
        this.state = {
            headerTitle: "",
            message: "",
            image: "",
        }
    }

    componentDidMount = () => {
        this.setState({
            headerTitle: this.props.headerTitle,
            message: this.props.message,
            image: this.props.image
          })
    }

  render() {
    const {headerTitle, image, message} = this.state
    return (  
              <div className="header-container">
                <IonTitle className="header-title"> {headerTitle} </IonTitle>
                <img alt="Imagen" src = {image} className="header-image"/>
                {
                    message === "" 
                    ? null 
                    : <IonLabel className="header-message"> {message}</IonLabel>
                }
              </div>
    );
  }
}

export default HeaderComponent;
