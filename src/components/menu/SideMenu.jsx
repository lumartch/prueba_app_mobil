import React, { Component } from 'react';
import { IonMenu, 
    IonToolbar, 
    IonContent, 
    IonItem, 
    IonTitle, 
    IonButtons,
    IonMenuButton, 
    IonLabel, 
    IonIcon, 
    IonMenuToggle } from '@ionic/react';
import { home, bookOutline, musicalNotesOutline, settingsSharp, logOutOutline } from 'ionicons/icons';
import MenuContent from './MenuContent';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {  logout } from '../../redux/actions/LoginActions';
import { clearProgress } from '../../redux/actions/ProgressActions';
import { clearSongs } from '../../redux/actions/SongsActions';
import store from '../../redux/store';
import './SideMenu.css';


const pages = [
    { title: 'Inicio', path: '/home',icon: home },
    { title: 'Biblioteca', path: '/library', icon: musicalNotesOutline },
    { title: 'Lecciones', path: '/lessons', icon: bookOutline }, 
    { title: 'Ajustes', path: '/settings', icon: settingsSharp}
];

const setting = [
    { path: '/profile' },
]

class SideMenu extends Component {
    constructor(){
        super()
        this.state = {
            path: "",
            disabled: true
        }
    }
    
    componentDidMount(){
        // Aquí poner la consulta al servidor del nickname del usuario
        this.setState({ 
            path: this.props.location.pathname,
            disabled: this._disableMenu(this.props.location.pathname.toLowerCase()) 
        })
    }

    componentDidUpdate(prevProps){
        if( prevProps.location.pathname !== this.props.location.pathname ){
            this.setState({ 
                path: this.props.location.pathname.toLowerCase(), 
                disabled: this._disableMenu(this.props.location.pathname.toLowerCase()) 
            })
        }
    }

   _disableMenu = (path) => {
        for(let i = 0; i < pages.length; i++){
            if(pages[i].path === path){
                return false
            }
        } 

        for(let i = 0; i < setting.length; i++){
            if(setting[i].path === path){
                return false
            }
        } 
        
        return true
    }

    render() {
        const { disabled } = this.state
        return (
            <IonMenu className="menu" side="end" disabled={ disabled } contentId="Main" type="overlay" mode="md">
                <IonToolbar className="menu-toolbar">
                    <IonMenuToggle>
                        <IonItem className="menu-profile" routerLink={ "/profile" } button>
                            <IonTitle className="menu-title">{ this.props.nickname } </IonTitle>
                        </IonItem>
                    </IonMenuToggle>
                    <IonButtons slot="end">            
                        <IonMenuButton color="white" className="menu-button" />
                    </IonButtons>
                </IonToolbar>
                <IonContent className="menu-content">
                {
                    pages.map( p => {
                        if(p.path !==  this.state.path){
                            return ( 
                                <MenuContent 
                                    key={p.title}  
                                    title={p.title} 
                                    path={p.path} 
                                    icon={p.icon}>
                                </MenuContent>
                                ) 
                            }  
                            return true; 
                    })
                }
                </IonContent>
                <Link to={"/login"} 
                    onClick={ () => {
                        store.dispatch(logout())
                        store.dispatch(clearProgress())
                        store.dispatch(clearSongs())
                        window.location.href="/login";
                        window.location.reload()
                        } 
                    }
                    className="link">
                    <IonItem className="menu-footer" button>
                        <IonLabel><label className="menu-footer-label">Cerrar Sesión</label></IonLabel>
                        <IonIcon className="menu-icon" icon={logOutOutline} />  
                    </IonItem>
                </Link>
            </IonMenu>
        ); 
    } 
}

const mapStateToProps = state => {
    return {
        nickname: state.user_state.user.nickname
    }
}

export default withRouter(connect(mapStateToProps)(SideMenu));