import { LINK_SERVER, IS_LOCAL } from "../../Const";

export const fetchFavorite = (correo, song_id, favorito) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();   
        var url = IS_LOCAL ? LINK_SERVER + "/songs/" : LINK_SERVER + "/songs/setFavorite.php";
        xhttp.open(IS_LOCAL ? "PUT" : "POST", url, true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                resolve(xhttp.responseText)
            } else {
                resolve(xhttp.responseText);
            }
        }
        let json = JSON.stringify({
            "correo": correo,
            "song_id": parseInt(song_id),
            "favorito": parseInt(favorito)
        })
        xhttp.send(json);
    })
}