import { IonAlert, IonContent } from '@ionic/react';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadSongs } from '../../redux/actions/SongsActions';
import store from '../../redux/store';
import Song from './Song';
import { fetchSongs } from './SongListConst';
import noSongs from '../../images/noSongs.png';
import noFavorites from '../../images/noFavorites.png';
import "./SongsList.css";

class SongsList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            update: false,
            opc: 0,
            showAlert: false,
            message : "",
            favorite: 0,
            tab: "all"
        }
    }

    async componentDidUpdate(prevProps, prevState) {
        if(prevProps.tab !== this.props.tab) {
            this.setState({
                update: true
            })
        }
        if(this.state.update === true) {
            this.setState({
                update: false
            })
            if(this.props.tab === "all") {
                this.setState({
                    opc: 0,
                    tab: "all"
                })
                let songs_state = await fetchSongs(this.props.user_state.user["correo"], "")
                await store.dispatch(loadSongs(songs_state))
            } else if(this.props.tab === "favorites"){
                this.setState({
                    opc: 1,
                    tab: "favorites"
                })
            } else if(this.props.tab === "finished"){
                this.setState({
                    opc: 2,
                    tab: "finished"
                })
            } else {
                this.setState({
                    opc: 3
                })
                let found = 0
                let rx = new RegExp(this.props.search, "i");
                Object.entries(this.props.songs_state.songs).forEach( (song, key) => {
                    if(song[1]["title"].match(rx) !== null || song[1]["author"].match(rx) !== null){
                        found += 1
                    }
                })         
                if( found === 0 ){
                    this._handleAlert(this._generateHTML("No se encontraron canciones. Intente de nuevo.", noSongs))
                }
            }
        }

        if(this.state.opc === 1){
            let favorite = 0
            Object.entries(this.props.songs_state.songs).forEach( (song, key) => {
                if(song[1]["favorite"] === 1){
                    favorite += 1
                }
            })                
            if( favorite === 0 ){
                this.setState({
                    opc: 0,
                    tab: "all"
                })
                this._handleAlert(this._generateHTML("No se encontraron canciones favoritas agregadas.", noFavorites))
            }
        }

        if(this.state.opc === 2){
            let finished = 0
            Object.entries(this.props.songs_state.songs).forEach( (song, key) => {
                if(song[1]["finished"] === 1){
                    finished += 1
                }
            })                
            if( finished === 0 ){
                this.setState({
                    opc: 0,
                    tab: "all"
                })
                this._handleAlert('<label class="alert-label-songs-generic">No se encontraron canciones terminadas aún. :B</label>')
            }
        }
    }

    _generateHTML = (myText, myImage) =>{
        return (`<div> 
                    <label class="alert-label-settings-update"> ${myText} </label> 
                    <img class="alert-img-settings" alt="${myImage}" src=${myImage} /> 
                </div>`)
    }
    
    _createSong = (song) => {
        if(song === null){
            return
        }
        return  <Song key={ song[0] } 
                    id={ song[0] } 
                    title={ song[1]["title"] } 
                    author={ song[1]["author"] } 
                    duration={ song[1]["duration"] } 
                    path={ song[1]["path"] }
                    favorite={ song[1]["favorite"] }
                    views={ song[1]["views"] }
                    rating={ song[1]["rating"] }
                    finished={ song[1]["finished"] }
                /> 
    }

    _readJSON = (json, opc) => {
        switch(opc){
            case 0:
                return Object.entries(json).map( (song, key) => {
                        return this._createSong(song)
                    }   
                )
            case 1:
                return Object.entries(json).map( (song, key) => {
                    if(song[1]["favorite"] === 1){
                        return this._createSong(song)
                    }
                    return null
                })
            case 2:
                return Object.entries(json).map( (song, key) => {
                    if(song[1]["finished"] === 1){
                        return this._createSong(song)
                    }
                    return null
                })
                
            case 3:
                let rx = new RegExp(this.props.search, "i");
                return Object.entries(json).map( (song, key) => {
                    if(song[1]["title"].match(rx) !== null || song[1]["author"].match(rx) !== null){
                        return this._createSong(song)
                    }
                    return null
                })
            default:
                
        }
    }



    _handleAlert = (message) => {
        this.setState({ showAlert: true, message: message })
    }
    
    render() {
        const { opc, showAlert, message, tab } = this.state
        return (
            <IonContent className="json-render">
                <IonAlert
                    isOpen={ showAlert }
                    onDidDismiss={ () => { 
                        this.setState({ showAlert: false }) 
                        this.props.func(tab)
                    } }
                    backdropDismiss = { false }
                    cssClass = 'alert-songs'
                    header = { "" }
                    subHeader = { "" }
                    message = { message }
                    buttons = { ["Ok"] }
                />
                {
                    this._readJSON(this.props.songs_state.songs, opc)
                }
            </IonContent>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state,
        songs_state: state.songs_state
    }
  }

export default connect(mapStateToProps)(SongsList);