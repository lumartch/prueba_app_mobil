import React, { Component } from 'react';
import { IonToolbar, IonButton, IonIcon } from '@ionic/react';
import { chevronBackOutline } from'ionicons/icons';
import './ContentHeader.css';

class ContentHeader extends Component {
    render() {
        return (
            <IonToolbar className="lessons-toolbar">
                <IonButton onClick={ this.props.goBack } className="back-header">
                    <IonIcon icon={ chevronBackOutline } color="white"/>
                </IonButton>
                <h1 className="title-font"> {this.props.lessonName} </h1>
            </IonToolbar>
        );
    }
}

export default ContentHeader;