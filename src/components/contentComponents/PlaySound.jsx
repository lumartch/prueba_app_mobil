import React, { Component } from 'react';
import { IonItem,  IonLabel } from '@ionic/react';
import "./PlaySound.css"

class PlaySound extends Component {
    constructor(props) {
        super()
        this.state = {
            soundDir: undefined,
            gifInfo: undefined,
            name: undefined,
            func: undefined
        }
        this.audio = new Audio();
    }

    componentDidMount() {
        if(this.props.gifInfo === undefined){
            this.setState({ 
                soundDir: this.props.soundDir,
                name: this.props.name
            })
        } else {
            this.setState({ 
                soundDir: this.props.soundDir,
                gifInfo: this.props.gifInfo,
                name: this.props.name,
                func: this.props.handleGif
            })
        }
        this.audio.src = this.props.soundDir;
    }

    _handleClick = () => {
        this.audio.play()
    }

    _handleClickState = () => {
        this.audio.play()
        this.state.func(this.state.gifInfo)
    }

    componentWillUnmount() {
        this.audio.src = null;
    }
    

    render() {
        return ( 
            <div className="playsound-container">
                {
                    this.props.gifInfo === undefined
                    ? (
                        <div>
                            <IonItem button className="sound-button" onClick={ this._handleClick }><img alt={this.props.iconPath} className="image-button" src={ this.props.iconPath }></img></IonItem >
                            <IonLabel className="button-footer"> { this.state.name } </IonLabel>
                        </div>
                    )
                    : (
                        <div>
                            <IonItem button className="sound-button" onClick={ this._handleClickState }><img alt={this.props.iconPath} className="image-button" src={ this.props.iconPath }></img></IonItem >
                            <IonLabel className="button-footer"> { this.state.name } </IonLabel>
                        </div>
                    )
                }
                
            </div>
        );
    }
}
export default PlaySound;