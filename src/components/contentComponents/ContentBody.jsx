import React, { Component } from 'react';
import {  
    IonItem, 
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonCardSubtitle,
    IonLabel,
    IonGrid,
    IonRow,
    IonImg,
    IonCol,
    IonButton,
    //IonIcon,
    IonTitle
    } from '@ionic/react';
import ReactPlayer from 'react-player';
import { Link } from 'react-router-dom';
import PlaySound from './PlaySound';
//import { playCircle } from 'ionicons/icons';
import { IS_LOCAL } from '../../Const';
import './ContentBody.css';

class ContentBody extends Component {
    constructor(props){
        super(props)
        this.state = {
            text: true,
            gifSource: undefined,
            gifFooter: undefined,
            activity: this.props.activity
        }
    }

    _loadGif = (src) => {
        this.setState({
            gifFooter: src[0],
            gifSource: src[1]
        })
    }

    _loadSounds = (sounds, gifs) => {
        if (sounds !== undefined ){
            return <IonRow className="sound-row">
                {
                    Object.entries(sounds).map( (sound, id) => {
                        return (
                            <IonCol className="sound-col" key={sound[0]}>
                                <PlaySound 
                                    name={ sound[0] } 
                                    soundDir={ sound[1][0] }
                                    iconPath={ sound[1][2] }
                                    gifInfo={ gifs[id] }
                                    handleGif={ this._loadGif }
                                />
                            </IonCol>
                        )
                    } )
                } 
            </IonRow> 
        }
    }

    _loadImages = (images) => {
        if (images !== undefined){
            return  <IonRow > 
                { 
                    Object.entries(images).map( img => {
                        return (
                            <IonCol className="img-col" key={ img[0] }>
                                <IonLabel className="img-title">
                                    { img[0] } 
                                    <IonImg className="img-lesson" src={ img[1] }/>
                                </IonLabel>
                            </IonCol>
                        )
                    } )
                } 
            </IonRow>
        }
    }

    _renderVideo(video){
        return(
            <IonCardContent className="item-video"> 
                <ReactPlayer 
                    url={ IS_LOCAL ? video[0] : video[1] }
                    playing={ false }
                    controls={ true }
                    width='100%'
                    height='100%'
                />
            </IonCardContent>
        )
    }

    _renderText(body){
        return (
            <div>
                <IonLabel 
                    className="label-content">
                        { body["content"] }
                </IonLabel>
                
                {
                    body["image"] !== undefined
                    ? (
                        <IonGrid>
                        { this._loadImages(body["image"]) }
                        </IonGrid>)
                    : null
                }
                
                {
                    body["sound"] !== null 
                    ? (
                        <IonGrid> 
                            { 
                                this._loadSounds(body["sound"], body["gif"]) 
                            } 
                        </IonGrid>
                    )
                    : null
                } 
                <IonLabel className="gif-footer">{ this.state.gifFooter }</IonLabel>
                <IonImg className="gif-lesson" src={ this.state.gifSource }/>
            </div>
        )
    }

    render() {
        const { video, host, body, lessonName, activity } = this.props
        return (
            <IonCard className="lesson-card-body">
                <IonCardHeader className="lesson-card-header">
                    <IonCardTitle className="lesson-card-title">
                        <IonButton className="lesson-narrator">
                            { /*<IonLabel className="lesson-narrator-icon">
                                <IonIcon icon={ playCircle }/>
                            </IonLabel>*/}
                        </IonButton>
                        Presentado por { host }
                    </IonCardTitle>
                    <IonCardSubtitle className="lesson-card-subtitle">
                        <IonLabel position="stacked">
                            { body["subtitle"] }
                        </IonLabel>
                    </IonCardSubtitle>
                </IonCardHeader>
                <IonCardContent className="lesson-card-content">
                    {
                        this.state.text
                        ? this._renderText(body)
                        : this._renderVideo(video)
                    }
                    <IonItem button className="lessons-resources" lines="none" onClick={ () => this.setState({text: !this.state.text})}>
                        <IonTitle className="resources">
                            Más recursos
                        </IonTitle>
                    </IonItem>
                    <Link to= { { 
                            pathname:`/lessonsactivity`,
                            state: {
                                lesson: lessonName,
                                header: this.props.header
                            }}}>
                        {
                            activity
                            ? <IonButton className="lesson-activity">Actividad</IonButton>
                            : null
                        }
                    </Link>
                </IonCardContent>
            </IonCard>
        );
    }
}

export default ContentBody;