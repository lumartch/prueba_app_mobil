import { IonAvatar, IonItem } from '@ionic/react';
import React, { Component } from 'react';
import {achievementsLessons, achievementsSongs} from './AchievementsConst';
import "./AchievementsList.css";

class AchievementsList extends Component {
    _renderLessonsAchievements = () =>{
        return achievementsLessons.map( (e, key) => {
            return  <IonItem className="achievement-container" key={key} disabled={this.props.finishedLessons >= e[0] ? false : true}>
                        <IonAvatar className="images-achievements"><img src={e[2]} alt={"achievement"}/></IonAvatar>
                        <label className="achievement-label">{e[1]}</label>
                    </IonItem>
        })
    } 
    
    _renderSongAchievements = () =>{
        return achievementsSongs.map( (e, key) => {
            return  <IonItem className="achievement-container" key={key} disabled={this.props.finishedSongs >= e[0] ? false : true}>
                        <IonAvatar className="images-achievements"><img src={e[2]} alt={"achievement"}/></IonAvatar>
                        <label className="achievement-label">{e[1]}</label>
                    </IonItem>
        })
    }   

    render() {
        return (
            <div>
                <IonItem className="achievement-header"><label className="header-label">Canciones</label></IonItem> 
                {
                    this._renderSongAchievements()
                }
                <IonItem className="achievement-header"><label className="header-label">Lecciones</label></IonItem>
                {
                    this._renderLessonsAchievements()
                }
            </div>
        );
    }
}

export default AchievementsList;