import React, { Component } from 'react';
import { IonPage,
    IonToolbar, 
    IonButtons, 
    IonMenuButton, 
    IonTitle, 
    IonContent,
    IonCard
     } from '@ionic/react';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { LessonsFetch } from '../components/lessonComponents/LessonsConst';
import store from '../redux/store';
import { loadProgress } from '../redux/actions/ProgressActions';
import { Link } from 'react-router-dom';
import Clef from '../images/clef.png';
import LittleStarSong from '../images/littleStarSong.png';
import FreePiano from '../images/piano.png';
import { loadSongs } from '../redux/actions/SongsActions';
import { fetchSongs } from '../components/songComponents/SongListConst';
import { achievementsLessons, achievementsSongs } from '../components/achievementsComponents/AchievementsConst';
import { setProfileFetch } from './profile/ProfileConst';
import splash from '../images/splash.png';
import { fetchLogin } from '../redux/actions/LoginActions';
import { loadLessons } from '../redux/actions/LessonsActions'; 
import { IAFetch } from '../components/IA/IAConst';
import './Home.css';


class Home extends Component {
    constructor(){
        super()
        this.state = {
            loading: true,
            counter: 0
        }
    }

    componentDidMount() {
        if(this.props.user_state.login) {
            this._loadingInfo()
        } else {
            window.location.reload()
        }
    }

    _loadingInfo = async () => {
        try{
            let user_data = await setProfileFetch(this.props.user_state.user["correo"])
            let progress_state = await LessonsFetch(this.props.user_state.user["correo"])
            let songs_state = await fetchSongs(this.props.user_state.user["correo"], "")
            let ia_state = await IAFetch(this.props.user_state.user["correo"])
            store.dispatch(loadProgress(progress_state))
            store.dispatch(loadSongs(songs_state))
            store.dispatch(fetchLogin(user_data))
            store.dispatch(loadLessons(ia_state))
            this.setState( { loading: false })
        } catch(err){

        }
    }

    render() {
        const { current } = this.props.progress_state
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> : 
            this.state.loading 
            ?   <IonPage> 
                    <div className="home-splash-div"> 
                        <h2 className="home-splash-title">Music Kids</h2>
                        <img alt={splash} src={splash} className="home-splash-img" />
                    </div>
                </IonPage>
            :   <IonPage>        
                    <IonContent className="home-background">
                        <IonCard className="home-card">
                            <IonToolbar className="home-toolbar">
                                <IonButtons>
                                    <IonTitle color="white"className="home-title">Bienvenido { this.props.user_state.user.name }</IonTitle>
                                    <IonMenuButton color="white" autoHide={ false } className="menu-button-home"/>
                                </IonButtons>
                            </IonToolbar>
                            <div className="home-avatar-container">
                                    <div className="home-avatar-text">
                                        <label className="home-text-position"> ¿Qué vamos a hacer hoy? 
                                        Estas son las 
                                        recomendaciones del día. </label> 
                                    </div>
                                    <div className="home-avatar-item">
                                        <div className="home-avatar-wrapper">
                                            <img className="home-avatar-img" src={`assets/avatars/${this.props.user_state.user.avatar}Home.png`} alt="img"/>
                                        </div>
                                    </div>
                            </div> 
                            <Link
                                className="link-lesson"
                                to={{ 
                                    pathname:`/game`,
                                    state: {
                                        path: this.props.songs_state.day_song.song.path, 
                                        soundfont: "acoustic_grand_piano",
                                        songName: this.props.songs_state.day_song.song.title,
                                        songId: this.props.songs_state.day_song.day
                                    }
                                }}>
                                <div className="home-container">
                                    <div className="home-first-text">Canción del día</div>
                                    <div className="home-second-text">¡Descubre los favoritos!</div>          
                                    <div className="home-container-wrapper">
                                        <img className="home-img" src={LittleStarSong} alt="img"/>
                                        <label className="home-title-text">{ this.props.songs_state.day_song.song.title }</label> 
                                    </div>
                                    <div className="home-third-text">¡Vamos a jugar!</div>
                                </div>
                            </Link>
                            <Link
                                className="link-lesson"
                                to={{ 
                                    pathname:`/pianosolo`,
                                    state: {
                                        path: this.props.songs_state.day_song.song.path, 
                                        soundfont: "acoustic_grand_piano",
                                        songName: this.props.songs_state.day_song.song.title,
                                        songId: this.props.songs_state.day_song.day
                                    }
                                }}>
                                <div className="home-container">
                                    <div className="home-first-text">... O sí lo prefieres</div>
                                    <div className="home-second-text">¡Juega en modo libre!</div>          
                                    <div className="home-container-wrapper">
                                        <img className="home-img" src={FreePiano} alt="img"/>
                                        <label className="home-title-text">Un piano libre para ti.</label> 
                                    </div>
                                    <div className="home-third-text">¡Vamos a jugar!</div>
                                </div>
                            </Link>
                            {
                                current !== "Lecciones terminadas" /*&& current !== "Sin internet"*/
                                    ?   <Link
                                            className="link-lesson"
                                            to={{ 
                                                pathname:`/lessonscontent`,
                                                state:{
                                                    header: current.header,
                                                    lesson: current.lesson
                                                }
                                            }}>
                                            <div className="home-container">
                                                <div className="home-first-text">Otro día otra lección</div> 
                                                <div className="home-second-text">Puede que te interese esta lección:</div>
                                                <div className="home-container-wrapper">
                                                    <img className="home-img" src={Clef} alt="img"/>
                                                    <label className="home-title-text">{ current.lesson }</label> 
                                                </div>
                                                <div className="home-third-text">¡Explora y aprende!</div>
                                            </div>
                                        </Link>
                                    :   <div className="home-container">
                                            <div className="home-first-text">Otro día otra lección</div> 
                                            <div className="home-second-text">Puede que te interese esta lección:</div>
                                            <div className="home-container-wrapper">
                                                <img className="home-img" src={Clef} alt="img"/>
                                                <label className="home-title-text">Lecciones terminadas</label> 
                                            </div>
                                            <div className="home-third-text">¡Explora y aprende!</div>
                                        </div>
                            }
                            <div className="home-container">
                                <div className="home-first-text">Paso a pasito</div> 
                                <div className="home-second-text">Próximo logro de lecciones:</div>
                                <div className="home-container-wrapper">
                                    <img className="home-img" src={
                                        this.props.progress_state.achievements < achievementsLessons.length
                                        ? achievementsLessons[this.props.progress_state.achievements][2] 
                                        : "assets/avatars/okay.png"} 
                                        alt="img"/>
                                    <label className="home-title-text">{   
                                        this.props.progress_state.achievements < achievementsLessons.length  
                                        ? achievementsLessons[this.props.progress_state.achievements][1] 
                                        : "Logros términados"}</label> 
                                </div>
                                <div className="home-third-text">¡Intenta y Gana!</div>
                            </div>
                            <div className="home-container">
                                <div className="home-first-text">... Dando Saltitos</div> 
                                <div className="home-second-text"> Próximo logro de canciones:</div>
                                <div className="home-container-wrapper">
                                    <img className="home-img" src={
                                        this.props.songs_state.achievements < achievementsSongs.length
                                        ? achievementsSongs[this.props.songs_state.achievements][2] 
                                        : "assets/avatars/okay.png"} 
                                        alt="img"/>
                                    <label className="home-title-text">{ 
                                        this.props.songs_state.achievements < achievementsSongs.length  
                                        ? achievementsSongs[this.props.songs_state.achievements][1] 
                                        : "Logros términados"}</label> 
                                </div>
                                <div className="home-third-text">¡Intenta y Gana!</div>
                            </div>
                            
                        </IonCard>
                    </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state,
        progress_state: state.progress_state,
        songs_state: state.songs_state
    }
}

export default connect(mapStateToProps)(Home);