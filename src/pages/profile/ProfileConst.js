import { LINK_SERVER, IS_LOCAL } from "../../Const";

export const setProfileFetch = async(correo) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();
        var url = IS_LOCAL ? LINK_SERVER + "/login/?correo=" + correo: LINK_SERVER + "/profile/getProfile.php";
        xhttp.open( IS_LOCAL ? "GET" : "POST", url, true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                let json = JSON.parse(xhttp.responseText)
                json.correo = correo
                resolve(json)
            } else {
                resolve(JSON.parse("{}"))
            }
        };
        let json = JSON.stringify({
            "correo": correo,
        })
        xhttp.send(IS_LOCAL ? null : json);
    })
}