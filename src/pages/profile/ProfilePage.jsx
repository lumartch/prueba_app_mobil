import React, { Component } from 'react';
import { IonPage,
    IonItem, 
    IonToolbar, 
    IonButtons, 
    IonMenuButton, 
    IonTitle, 
    IonContent,
    IonCard, 
    IonCardContent, 
    IonLabel,
    IonGrid, 
    IonCol,
    IonRow, 
    IonIcon, 
    IonAvatar,
    IonAlert } from '@ionic/react';
import Avatar from '../../components/newAccount/Avatar';
import { bookOutline, flashOutline, gameControllerOutline, musicalNotesOutline } from 'ionicons/icons';
import Clef from '../../images/clef.png';
import LogrosDesbloqueados from '../../images/Logros desbloqueados.png';
import { Link, Redirect } from 'react-router-dom';
import AchievementsList from '../../components/achievementsComponents/AchievementsList';
import { connect } from 'react-redux';
import './ProfilePage.css'

class ProfilePage extends Component {
    constructor() {
        super()
        this.state = {
            showAchievements: false,
            alertMessage: undefined,
            showAlert: false
        }
    }

    componentDidMount(){
        if(!this.props.user_state.login){
            return 
        }
    }

    _getLevel = () => {
        let level = 100/(Object.keys(this.props.songs_state.songs).length + 45)
        return Math.trunc(level*(this.props.songs_state.finished + this.props.progress_state.finished))
    }

    render() {
        const { current } = this.props.progress_state
        const { avatar } = this.props.user_state.user
        const { showAchievements, showAlert, alertMessage } = this.state;
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> : 
            <IonPage>
                <IonAlert
                    isOpen={ showAlert }
                    onDidDismiss={ () => this.setState({ showAlert: false }) }
                    backdropDismiss = { false }
                    cssClass = 'alert-message'
                    subHeader = { "" }
                    message = { alertMessage }
                    buttons = { ["Ok"] }/>
                <IonContent className="profile-background">
                    <IonToolbar className="profile-toolbar">
                        <IonButtons>
                            <IonTitle color="white"className="profile-title">MI PERFIL</IonTitle>
                            <IonMenuButton color="white" autoHide={ false } className="menu-button-profile"/>
                        </IonButtons>
                    </IonToolbar>
                    <IonCard className="profile-card">
                        <IonCardContent>
                            <div className="settings-avatar">
                                <Avatar src={"assets/avatars/" + avatar + ".jpg"}></Avatar>
                            </div>
                            <IonItem className="profile-info-container"> 
                                <IonGrid className="profile-grid">
                                    <IonRow>
                                        <IonCol>
                                            <IonLabel><IonIcon icon={flashOutline} /> Nivel: { this._getLevel() } </IonLabel>
                                        </IonCol>
                                        <IonCol>
                                            <IonLabel><IonIcon icon={gameControllerOutline} /> Logros: { this.props.songs_state.achievements + this.props.progress_state.achievements }</IonLabel>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow>
                                        <IonCol>
                                            <IonLabel>
                                                <IonIcon icon={musicalNotesOutline} /> Terminadas: { this.props.songs_state.finished }
                                            </IonLabel>
                                        </IonCol>
                                        <IonCol>
                                            <IonLabel>
                                                <IonIcon icon={bookOutline} /> Terminadas: {this.props.progress_state.finished}
                                            </IonLabel>
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>    
                            </IonItem>
                            {
                                current !== "Lecciones terminadas" && current !== "Sin internet"
                                ?   <Link
                                        to={{ 
                                            pathname:`/lessonscontent`,
                                            state:{
                                                header: current.header,
                                                lesson: current.lesson
                                            }
                                        }}
                                        className="link-lesson">
                                        <IonItem className="current-lesson-item" button>
                                            <IonAvatar className="images"><img src={Clef} alt="lesson"/></IonAvatar>
                                            <IonLabel className="labels">{current.lesson}</IonLabel>
                                        </IonItem>
                                    </Link>
                                :   <IonItem className="current-lesson-item">
                                        <IonAvatar className="images"><img src={Clef} alt="lesson"/></IonAvatar>
                                        <IonLabel className="labels">{current}</IonLabel>
                                    </IonItem>
                                }
                            <IonItem className="achievement-item" button onClick={() => this.setState({ showAchievements: !this.state.showAchievements })}>
                                <IonAvatar className="images"><img src={LogrosDesbloqueados} alt="achievements"/></IonAvatar>
                                <IonLabel className="labels">Logros desbloqueados</IonLabel>
                            </IonItem>
                            {
                                showAchievements 
                                ? <AchievementsList finishedSongs={ this.props.songs_state.finished } finishedLessons={ this.props.progress_state.finished }/>
                                : null
                            }
                        </IonCardContent>
                    </IonCard>
                </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state,
        progress_state: state.progress_state,
        songs_state: state.songs_state
    }
  }

export default connect(mapStateToProps)(ProfilePage);