import { LINK_SERVER, IS_LOCAL } from "../../Const";

export const setSettingsFetch = (correo, password, nickname, name, phone, avatar) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();   
        var url = IS_LOCAL ? LINK_SERVER + "/settings/" : LINK_SERVER + "/settings/setSettings.php";
        xhttp.open(IS_LOCAL ? "PUT" : "POST", url, true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                let json = JSON.stringify({ 
                    "avatar": avatar,
                    "correo": correo, 
                    "nickname": nickname, 
                    "name": name,
                    "phone": phone
                });
                resolve(JSON.parse(json))
            } else {
                resolve(false);
            }
        };
        var json = JSON.stringify({ 
            "correo": correo, 
            "password": password, 
            "nickname": nickname, 
            "nombre": name,
            "telefono": phone,
            "avatar": avatar
        });
        xhttp.send(json);
    })
}